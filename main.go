package main

import (
	"fmt"
	"log"
	"time"
)

package main

import (
//"fmt"
"github.com/gocql/gocql"
"time"
)

type Table struct{
	Id string
	UserId string
	Payload string
}

func main() {
	cluster := gocql.NewCluster("localhost")
	cluster.Consistency = gocql.Quorum
	cluster.Keyspace = "demo"
	cluster.ProtoVersion = 4
	cluster.SocketKeepalive = 10 * time.Second
	client, err := cluster.CreateSession()
	if err != nil{
		panic(err.Error())
	}

	var batchSz = 1000
	batch := client.NewBatch(gocql.LoggedBatch)

	query := "INSERT INTO table1 (id, user_id, payload) VALUES (?, ?, ?)"
	tables := []Table{}
	for i, table := range tables{
		batch.Query(
			query,
			table.Id,
			table.UserId,
			table.Payload,
		)
		if(i + 1)%batchSz == 0{
			err := client.ExecuteBatch(batch)
			if err != nil{
				panic(err.Error())
			}
			batch := client.NewBatch(gocql.LoggedBatch)
		}
	}
	err = client.ExecuteBatch(batch)
	if err != nil{
		panic(err.Error())
	}

	var id string
	var userID string
	var payload string

	iter := client.Query("SELECT * FROM table1 WHERE id = ?", "6ba7b810-9dad-11d1-80b4-00c04fd430c8").Iter()
	for iter.Scan(&id, &userID, &payload){
		fmt.Println("Row 1: ", id, userID, payload)
	}
	if err := iter.Close(); err != nil {
		log.Fatal(err)
	}
	// err = client.Query("INSERT INTO table1 (id, user_id, payload) VALUES (?, ?, ?)",
	//   "6ba7b810-9dad-11d1-80b4-00c04fd430c8",
	//   "2",
	//   "asd").
	//   Exec()
	// if err != nil{
	//   panic(err.Error())
	// }
	// err = client.Query("INSERT INTO table1 (id, user_id, payload) VALUES (?, ?, ?)",
	//   "6ba7b810-9dad-11d1-80b4-00c04fd430c8",
	//   "3",
	//   "asd").
	//   Exec()
	// if err != nil{
	//   panic(err.Error())
	// }

	// var id string
	// var userID string
	// var postData string

	//err = client.Query("SELECT * FROM table1 WHERE id = ?", "6ba7b810-9dad-11d1-80b4-00c04fd430c8").Scan(&id, &userID, &postData)
	//err = client.Query("DELETE FROM table1 WHERE id = ?", "6ba7b810-9dad-11d1-80b4-00c04fd430c8").Exec()
	//if err != nil{
	//  panic(err.Error())
	//}
	//fmt.Println(id, userID, postData)

}